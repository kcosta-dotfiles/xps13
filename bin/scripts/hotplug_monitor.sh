
# Detect if the monitor is connected and, if so, the monitor's ID:
conHdmi=$(xrandr | sed -n '/^DP1 connected/p')

if [ -n "$conHdmi" ]; then
    echo "DP1 is connected."
    xrandr --output eDP-1 --mode 1920x1080_60.00 --output DP1 --auto --right-of eDP1
    gsettings set org.gnome.desktop.interface text-scaling-factor 1
    xrandr --dpi 96
else
    echo "DP1 is NOT connected."
    xrandr --output eDP-1 --mode 3200x1800 --output DP1 --off
    gsettings set org.gnome.desktop.interface text-scaling-factor 1.75
    xrandr --dpi 196
fi
